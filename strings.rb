#exercise 2.1.1
puts "I am hungry.\tfoodie me"
puts "I am hungry.\nfoodie me"
puts 'I am hungry.\tfoodie me'
puts 'I am hungry.\nfoodie me'

#concatenation
first_name = 'Aashma'
last_name = 'Dahal'
puts first_name
puts last_name
puts "Aashma" + "Dahal"

#single quoted strings
'#{first_name} #{last_name}'

#interpolation
city = 'Kathmandu'
state = 'Bagmati'
puts "#{city}, #{state}"
puts "#{city},  #{state}"

#print
puts "hello, world!"


#attributes
puts "apple".length 

#boolean values

puts password = "ad"

if (password.length < 6)
    puts"Password is too short."
end

#No paranthesis
if password.length < 6
    puts"Password is too short."
end

password = "abc"
puts"Password is too short." if password.length < 6
    
#combining and inverting booleans
a = "tree"
b = ""
if a.length == 0 && b.length == 0
    puts "Both strings are empty!"
else
   puts "At least one of the strings is non empty."
 end


if a.length == 0 || b.length == 0
        puts"At least one of the strings is empty!"
else
    puts "Neither of the strings is empty."
end

!puts(a.length == 0)

x = "foo"
y = ""
puts !!("foo")

#string iteration
p="soliloque"
for i in 0..(p.length) 
    puts p[i]
end
for i in 0..(p.length)
    puts p[p.length-i]
end