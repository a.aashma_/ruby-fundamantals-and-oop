#boolean methods
x=""
y=""
if x.empty? && y.empty?
    puts "Both strings are empty!"
else
    puts "At least one of the strings is nonempty."
end

#downcase
puts "SUPERMAN".downcase

puts fruit="PINEAPPLE"
puts fruit.downcase
puts fruit

#uppercase
puts first_name="Aashma"
puts username=first_name.downcase
puts"#{username}@example.com" 
puts last_name="dahal"
puts last_name.upcase

puts "hello".include? "lo"
puts "hello".include? "ol"

#exercise
string="hoNeY BaDGer"
if  string.downcase.include? "badger" then
    puts "true"
end
  

a="kathmandu"
puts a.upcase
b="POKHARA" 
puts b.upcase

puts "".empty?

